module.exports = {
  root: true,
  extends: '@react-native-community',
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'jest', 'detox'],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/no-shadow': ['error'],
        'no-shadow': 'off',
        'no-undef': 'off',
        'react-hooks/exhaustive-deps': 'off',
        'eslint-comments/no-unused-disable': 'off',
      },
    },
  ],
  parserOptions: {
    warnOnUnsupportedTypeScriptVersion: false,
  },
  env: {
    jest: true,
  },
};
