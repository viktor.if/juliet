module.exports = {
  bracketSpacing: true,
  printWidth: 80,
  semi: true,
  singleQuote: true,
  tabWdth: 2,
  trailingComma: 'all',
};
