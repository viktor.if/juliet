# Juliet

Online grocery store

## Environment

- React Native (v0.67)
- React (v17)
- React Navigation (v6)
- Supabase (v1.33)
- RN Storybook (5.3)

## How to run app locally

In the terminal

```bash
# 1. Install React Native dependencies
yarn install

# 2. Install cocoapods
brew install cocoapods

# 3. Install Pod dependencies
# for Intel chip:
npx pod-install
# for Apple M1 chip:
# step 1
sudo arch -x86_64 gem install ffi
# step 2
arch -x86_64 pod install

# 4. Run app on the platform locally
# on Android
yarn run android
# on iOS
# step 1: open xcworkspace with xCode
cd ios && open *.xcworkspace
# step 2: choose and start the scheme
```

## How to run storybook

1. Open the .env file and set **LOAD_STORYBOOK** variable to **true**

2. Then enter into the terminal

```bash
# 1. Start metro server
yarn start

# 2. Start storybook
yarn storybook

# 3. Run app on the platform locally
# on Android
yarn run android
# on iOS
# step 1: open xcworkspace with xCode
cd ios && open *.xcworkspace
# step 2: choose and start the scheme
```
