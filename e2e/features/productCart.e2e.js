/* eslint-env detox/detox, jest */

const { default: productListPage } = require('../pageObjects/ProductListPage');
const { default: productCartPage } = require('../pageObjects/ProductCartPage');
const { default: commonPage } = require('../pageObjects/commonPage');

describe('Cart', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should be visible', async () => {
    await productListPage.expectProductListToBeVisible();
    await productListPage.userTypesIntoSearchBar('sweet');
    await productListPage.userTapsProductAddButton();
    await commonPage.userTapsHeaderCartButton();
    await productCartPage.expectProductListToBeVisible();
  });

  it('should allow to change a number of products in the cart', async () => {
    await productCartPage.userTapsPlusButton();
    await productCartPage.userTapsMinuButton();
  });

  it('should enable checkout', async () => {
    await productCartPage.userTapsCheckoutButton();
    await productCartPage.userTapsAlertAcceptButton();
  });

  it('should allow to remove all products from the cart', async () => {
    await productCartPage.userTapsMinuButton();
    await commonPage.expectListEmptyToBeVisible();
  });

  it('should allow to go to the home screen', async () => {
    await commonPage.userTapsHeaderHomeButton();
  });
});
