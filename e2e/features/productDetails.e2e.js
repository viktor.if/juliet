/* eslint-env detox/detox, jest */

const { default: productListPage } = require('../pageObjects/ProductListPage');
const {
  default: productDetailsPage,
} = require('../pageObjects/ProductDetailsPage');
const { default: commonPage } = require('../pageObjects/commonPage');

describe('Product details', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should be visible', async () => {
    await productListPage.expectProductListToBeVisible();
    await productListPage.userTypesIntoSearchBar('sweet');
    await productListPage.userTapsProductImage();
    await productDetailsPage.expectProductListToBeVisible();
  });

  it('should allow go to the cart', async () => {
    await commonPage.userTapsHeaderCartButton();
    await commonPage.userTapsHeaderArrowLeftButton();
  });

  it('should be scrollable', async () => {
    await productDetailsPage.userScrollsDown(200);
    await productDetailsPage.userScrollsUp(200);
  });

  it('should allow to add and remove product from the cart', async () => {
    await productDetailsPage.userTapsAddButton();
    await productDetailsPage.userTapsPlusButton();
    await productDetailsPage.userTapsMinusButton();
    await productDetailsPage.userTapsMinusButton();
  });

  it('should allow go back', async () => {
    await commonPage.userTapsHeaderCloseButton();
  });
});
