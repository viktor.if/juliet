/* eslint-env detox/detox, jest */

const { default: productListPage } = require('../pageObjects/ProductListPage');
const { default: commonPage } = require('../pageObjects/CommonPage');

describe('Product list', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  it('should be visible', async () => {
    await productListPage.expectProductListToBeVisible();
  });

  it('should be scrollable', async () => {
    await productListPage.userScrollsDown(200);
    await productListPage.userScrollsUp(200);
  });

  it('after successful searching a product, should contain found product', async () => {
    await productListPage.userTypesIntoSearchBar('sweet');
    await productListPage.expectProductItemToBeVisible();
  });

  it('should display items in the Favorites mode', async () => {
    await productListPage.userTapsHeartButtonOnProductItem();
    await productListPage.userTapsHeartButtonOnHeader();
    await productListPage.expectProductItemToBeVisible();
  });

  it('should be empty', async () => {
    await productListPage.userTapsHeartButtonOnHeader();
    await productListPage.userTypesIntoSearchBar('non existing product');
    await commonPage.expectListEmptyToBeVisible();
  });
});
