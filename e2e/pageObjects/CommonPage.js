/* eslint-env detox/detox, jest */

class CommonPage {
  get headerCloseButton() {
    return element(by.id('header-close-button'));
  }

  get headerCartButton() {
    return element(by.id('header-cart-button'));
  }

  get headerArrowLeftButton() {
    return element(by.id('header-arrow-left-button'));
  }

  get headerHomeButton() {
    return element(by.id('home-button'));
  }

  get listsEmpty() {
    return element(by.id('product-list-empty'));
  }

  async userTapsHeaderCloseButton() {
    await this.headerCloseButton.tap();
  }

  async userTapsHeaderCartButton() {
    await this.headerCartButton.tap();
  }

  async userTapsHeaderArrowLeftButton() {
    await this.headerArrowLeftButton.tap();
  }

  async userTapsHeaderHomeButton() {
    await this.headerHomeButton.tap();
  }

  async expectListEmptyToBeVisible() {
    await expect(this.listsEmpty).toBeVisible();
  }
}

module.exports = {
  default: new CommonPage(),
};
