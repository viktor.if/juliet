/* eslint-env detox/detox, jest */

class ProductCartPage {
  get page() {
    return element(by.id('cart-screen'));
  }

  get minusButton() {
    return element(by.id('cart-list-item-minus-button-2'));
  }

  get plustButton() {
    return element(by.id('cart-list-item-plus-button-2'));
  }

  get checkoutButton() {
    return element(by.id('cart-checkout-bar-checkout-button'));
  }

  get checkoutAlertAcceptButton() {
    if (device.getPlatform() === 'ios') {
      return element(by.type('_UIAlertControllerActionView'));
    }

    return element(by.type('android.widget.Button').and(by.text('OK')));
  }

  async expectProductListToBeVisible() {
    await waitFor(this.page).toBeVisible().withTimeout(3000);
  }

  async userTapsPlusButton() {
    await this.plustButton.tap();
  }

  async userTapsMinuButton() {
    await this.minusButton.tap();
  }

  async userTapsCheckoutButton() {
    await this.checkoutButton.tap();
  }

  async userTapsAlertAcceptButton() {
    await this.checkoutAlertAcceptButton.tap();
  }
}

module.exports = {
  default: new ProductCartPage(),
};
