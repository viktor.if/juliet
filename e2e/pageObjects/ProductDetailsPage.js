/* eslint-env detox/detox, jest */

class ProductListPage {
  get page() {
    // return element(by.id('product-details'));
    return element(by.id('product'));
  }

  get scrollView() {
    return element(by.id('product-details-scroll'));
  }

  get addButton() {
    return element(by.id('product-details-add-button'));
  }

  get minusButton() {
    return element(by.id('product-details-minus-button'));
  }

  get plusButton() {
    return element(by.id('product-details-plus-button'));
  }

  async expectProductListToBeVisible() {
    await waitFor(this.page).toBeVisible().withTimeout(3000);
  }

  async userScrollsUp(offset) {
    await this.scrollView.scroll(offset, 'up');
  }

  async userScrollsDown(offset) {
    await this.scrollView.scroll(offset, 'down');
  }

  async userTapsAddButton() {
    await this.addButton.tap();
  }

  async userTapsMinusButton() {
    await this.minusButton.tap();
  }

  async userTapsPlusButton() {
    await this.plusButton.tap();
  }
}

module.exports = {
  default: new ProductListPage(),
};
