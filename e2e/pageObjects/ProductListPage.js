/* eslint-env detox/detox, jest */

class ProductListPage {
  get productListPage() {
    return element(by.id('product-list'));
  }

  get searchBar() {
    return element(by.id('search-bar-text-input'));
  }

  get productItem() {
    return element(by.id('product-list-item-2'));
  }

  get productItemHeartButton() {
    return element(by.id('product-list-item-favorite-2'));
  }

  get headerFavoritesButton() {
    return element(by.id('header-favorites-button'));
  }

  get productItemImage() {
    return element(by.id('product-list-item-image-2'));
  }

  get productItemAddButton() {
    return element(by.id('product-list-item-add-button-2'));
  }

  get productItemMinusButton() {
    return element(by.id('product-list-item-minus-button-2'));
  }

  get productItemPlusButton() {
    return element(by.id('product-list-item-plus-button-2'));
  }

  async expectProductListToBeVisible() {
    await waitFor(this.productListPage).toBeVisible().withTimeout(3000);
  }

  async userScrollsUp(offset) {
    await this.productListPage.scroll(offset, 'up');
  }

  async userScrollsDown(offset) {
    await this.productListPage.scroll(offset, 'down');
  }

  async userTypesIntoSearchBar(text) {
    await this.searchBar.replaceText(text);
  }

  async expectProductItemToBeVisible() {
    await expect(this.productItem).toBeVisible();
  }

  async userTapsHeartButtonOnProductItem() {
    await this.productItemHeartButton.tap();
  }

  async userTapsHeartButtonOnHeader() {
    await this.headerFavoritesButton.tap();
  }

  async userTapsProductImage() {
    await this.productItemImage.tap();
  }

  async userTapsProductAddButton() {
    await this.productItemAddButton.tap();
  }
}

module.exports = {
  default: new ProductListPage(),
};
