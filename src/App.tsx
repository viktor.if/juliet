import Config from 'react-native-config';
import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { enableFreeze } from 'react-native-screens';

import './utils/ignoredLogs';
import AppStackContainer from './containers/navigators/AppStackContainer';
import StorybookUI from '../storybook';
import { store, persistor } from './store';

enableFreeze(true);

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppStackContainer />
      </PersistGate>
    </Provider>
  );
};

export default Config.LOAD_STORYBOOK === 'true' ? StorybookUI : App;
