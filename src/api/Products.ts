import { supabase } from '../utils/supabase';
import { PRODUCTS } from '../utils/constants';
import { TProduct } from '../duck/productAll/types';
import { PostgrestError } from '@supabase/supabase-js';

export default class ProductsApi {
  /** Get all products */
  static getAll = async ({
    query,
  }: {
    query: string;
  }): Promise<{
    count: number;
    data: TProduct[];
    error: PostgrestError | null;
  }> => {
    const { data, count, error } = query
      ? await supabase
          .from(PRODUCTS)
          .select('*', { count: 'exact' })
          .ilike('name', `%${query}%`)
      : await supabase.from(PRODUCTS).select('*', { count: 'exact' });

    return {
      data: data || [],
      count: count || 0,
      error,
    };
  };

  /** Get favorite products */
  static getFavorites = async ({
    ids,
  }: {
    ids: number[];
  }): Promise<{
    data: TProduct[];
    error: PostgrestError | null;
  }> => {
    const { data, error } = await supabase
      .from(PRODUCTS)
      .select('*', { count: 'exact' })
      .in('id', ids);

    return {
      data: data || [],
      error,
    };
  };
}
