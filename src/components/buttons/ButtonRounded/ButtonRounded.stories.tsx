import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import ButtonRounded from './ButtonRounded';
import CenterView from '../../decorators/CenterView';
import { TButtonRoundedProp } from './types';

const defaultProps: TButtonRoundedProp = {
  disabled: false,
  onPress: action('onPress'),
  pale: false,
  small: false,
  text: 'Rounded button',
  uppercased: false,
};

storiesOf('Buttons / Rounded', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('default', () => <ButtonRounded {...defaultProps} />)
  .add('disabled', () => <ButtonRounded {...defaultProps} disabled />)
  .add('uppercased', () => <ButtonRounded {...defaultProps} uppercased />)
  .add('pale', () => <ButtonRounded {...defaultProps} pale />)
  .add('small', () => <ButtonRounded {...defaultProps} small />);
