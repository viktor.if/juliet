import React from 'react';
import { TButtonRoundedProp } from './types';
import styles from './styles';

import {
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';

export default ({
  disabled = false,
  uppercased = false,
  innerContainerStyle: innerStyleProp,
  onPress,
  outterContainerStyle: outterStyleProp,
  pale = false,
  small = false,
  text,
  textStyle: textStyleProp,
  testID,
}: TButtonRoundedProp) => {
  const outterContainerStyle: StyleProp<ViewStyle> = [styles.outterContainer];
  const innerContainerStyle: StyleProp<ViewStyle> = [
    styles.innerContainer,
    disabled && styles.innerContainerDisabled,
  ];
  const textStyle: StyleProp<TextStyle> = [styles.text];

  if (pale) {
    innerContainerStyle.push(styles.containerPale);
    textStyle.push(styles.textPale);
  }

  if (small) {
    innerContainerStyle.push(styles.containerSmall);
    textStyle.push(styles.textSmall);
  }

  if (uppercased) {
    textStyle.push(styles.textUppercased);
  }

  outterContainerStyle.push(outterStyleProp);
  innerContainerStyle.push(innerStyleProp);
  textStyle.push(textStyleProp);

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={outterContainerStyle}
      testID={testID}
    >
      <View style={innerContainerStyle}>
        <Text style={textStyle}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};
