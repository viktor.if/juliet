import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  /** Container */
  outterContainer: {
    paddingHorizontal: 4,
  },
  innerContainer: {
    alignSelf: 'center',
    backgroundColor: '#fc8f00',
    borderRadius: 100,
    justifyContent: 'center',
    paddingHorizontal: Layout.baseMarging,
    paddingVertical: 12,
  },
  innerContainerDisabled: {
    backgroundColor: '#ffca84',
  },
  containerSmall: {
    paddingVertical: 8,
    paddingHorizontal: 12,
  },
  containerPale: {
    backgroundColor: '#fae5ca',
  },

  /** Text */
  text: {
    color: '#fff',
    fontSize: RFValue(14, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(18, BASE_SCREEN_HEIGHT),
  },
  textSmall: {
    fontSize: RFValue(12, BASE_SCREEN_HEIGHT),
    lineHeight: RFValue(16, BASE_SCREEN_HEIGHT),
  },
  textPale: {
    color: '#fc8f00',
  },
  textUppercased: {
    textTransform: 'uppercase',
  },
});
