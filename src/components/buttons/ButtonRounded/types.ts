import { StyleProp, ViewStyle, TextStyle } from 'react-native';

export type TButtonRoundedProp = {
  disabled?: boolean;
  uppercased?: boolean;
  innerContainerStyle?: StyleProp<ViewStyle>;
  onPress: () => void;
  outterContainerStyle?: StyleProp<ViewStyle>;
  pale?: boolean;
  small?: boolean;
  text: string;
  textStyle?: StyleProp<TextStyle>;
  testID?: string;
};
