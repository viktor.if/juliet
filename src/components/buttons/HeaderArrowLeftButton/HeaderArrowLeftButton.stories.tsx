import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';
import React from 'react';

import HeaderArrowLeftButton from './HeaderArrowLeftButton';
import CenterView from '../../decorators/CenterView';

storiesOf('Buttons / Header arrow left', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <HeaderArrowLeftButton onPress={action('onHeaderArrowLeftButtonPress')} />
  ));
