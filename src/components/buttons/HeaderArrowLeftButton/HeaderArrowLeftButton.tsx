import React from 'react';
import { TouchableOpacity } from 'react-native';

import styles from './styles';
import { THeaderArrowLeftButtonProp } from './types';

// Icons
import ArrowLeftIcon from '../../../../assets/svgs/arrow-left.svg';

export default ({ onPress }: THeaderArrowLeftButtonProp) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      testID="header-arrow-left-button"
    >
      <ArrowLeftIcon fill="#333359" height={24} width={24} />
    </TouchableOpacity>
  );
};
