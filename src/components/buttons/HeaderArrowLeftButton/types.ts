export type THeaderArrowLeftButtonProp = {
  onPress: () => void;
};
