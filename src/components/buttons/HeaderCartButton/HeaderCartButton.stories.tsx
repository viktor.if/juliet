import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';
import React from 'react';

import HeaderCartButton from './HeaderCartButton';
import CenterView from '../../decorators/CenterView';
import { THeaderCartButtonProp } from './types';

const defaultProps: THeaderCartButtonProp = {
  disabled: false,
  onPress: action('onPress'),
  qtyProdsInCart: 0,
};

storiesOf('Buttons / Header cart', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('empty', () => <HeaderCartButton {...defaultProps} />)
  .add('filled', () => (
    <HeaderCartButton {...defaultProps} qtyProdsInCart={1} />
  ));
