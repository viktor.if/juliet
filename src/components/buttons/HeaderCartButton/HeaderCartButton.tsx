import React from 'react';
import { TouchableOpacity } from 'react-native';

import styles from './styles';
import { THeaderCartButtonProp } from './types';

// Icons
import BasketIcon from '../../../../assets/svgs/basket.svg';
import BasketOutlineIcon from '../../../../assets/svgs/basket-outline.svg';

export default ({
  qtyProdsInCart,
  onPress,
  disabled = false,
}: THeaderCartButtonProp) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={styles.container}
      testID="header-cart-button"
    >
      {qtyProdsInCart ? (
        <BasketIcon fill="#fda00c" height={24} width={24} />
      ) : (
        <BasketOutlineIcon fill="#fda00c" height={24} width={24} />
      )}
    </TouchableOpacity>
  );
};
