export type THeaderCartButtonProp = {
  disabled?: boolean;
  qtyProdsInCart: number;
  onPress: () => void;
};
