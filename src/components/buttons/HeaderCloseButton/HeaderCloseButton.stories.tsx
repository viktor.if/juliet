import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import HeaderCloseButton from './HeaderCloseButton';
import CenterView from '../../decorators/CenterView';

storiesOf('Buttons / Header close', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <HeaderCloseButton onPress={action('onHeaderCloseButtonPress')} />
  ));
