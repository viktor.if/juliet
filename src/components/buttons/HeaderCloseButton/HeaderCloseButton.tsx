import React from 'react';
import { TouchableOpacity } from 'react-native';

import styles from './styles';
import { THeaderCloseButtonProp } from './types';

// Icons
import CloseIcon from '../../../../assets/svgs/close.svg';

export default ({ onPress }: THeaderCloseButtonProp) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      testID="header-close-button"
    >
      <CloseIcon fill="#333359" height={24} width={24} />
    </TouchableOpacity>
  );
};
