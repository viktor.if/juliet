import { StyleSheet } from 'react-native';
import { Layout } from '../../../styles';

export default StyleSheet.create({
  container: {
    paddingHorizontal: Layout.baseMarging,
    paddingVertical: 12,
  },
});
