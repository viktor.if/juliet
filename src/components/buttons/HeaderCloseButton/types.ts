export type THeaderCloseButtonProp = {
  onPress: () => void;
};
