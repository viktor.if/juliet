import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import HeaderFavoritesButton from './HeaderFavoritesButton';
import CenterView from '../../decorators/CenterView';
import { THeaderFavoritesButtonProp } from './types';

const defaultProps: THeaderFavoritesButtonProp = {
  onPress: action('onPress'),
  toggled: false,
};

storiesOf('Buttons / Header favorite', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('toggled off', () => <HeaderFavoritesButton {...defaultProps} />)
  .add('toggled on', () => <HeaderFavoritesButton {...defaultProps} toggled />)
  .add('disabled', () => <HeaderFavoritesButton {...defaultProps} disabled />);
