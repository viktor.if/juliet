import React from 'react';
import { TouchableOpacity } from 'react-native';

import styles from './styles';
import { THeaderFavoritesButtonProp } from './types';

// Icons
import HeartIcon from '../../../../assets/svgs/heart.svg';
import HeartOutlineIcon from '../../../../assets/svgs/heart-outline.svg';

export default ({ toggled, onPress, disabled }: THeaderFavoritesButtonProp) => (
  <TouchableOpacity
    disabled={disabled}
    onPress={onPress}
    style={styles.container}
    testID="header-favorites-button"
  >
    {toggled ? (
      <HeartIcon fill="#ff2b4d" height={24} width={24} />
    ) : (
      <HeartOutlineIcon fill="#adadbe" height={24} width={24} />
    )}
  </TouchableOpacity>
);
