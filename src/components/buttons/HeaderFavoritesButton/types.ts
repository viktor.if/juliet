export type THeaderFavoritesButtonProp = {
  disabled?: boolean;
  toggled: boolean;
  onPress: () => void;
};
