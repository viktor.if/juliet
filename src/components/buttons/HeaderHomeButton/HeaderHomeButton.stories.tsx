import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import HeaderHomeButton from './HeaderHomeButton';
import CenterView from '../../decorators/CenterView';

storiesOf('Buttons / Header home', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <HeaderHomeButton onPress={action('onHeaderHomeButtonPress')} />
  ));
