import React from 'react';
import { TouchableOpacity } from 'react-native';

import styles from './styles';
import { THeaderHomeButtonProp } from './types';

// Icons
import HomeIcon from '../../../../assets/svgs/home.svg';

export default ({ onPress }: THeaderHomeButtonProp) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      testID="home-button"
    >
      <HomeIcon fill="#fda00c" height={24} width={24} />
    </TouchableOpacity>
  );
};
