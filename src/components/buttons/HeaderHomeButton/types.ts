export type THeaderHomeButtonProp = {
  onPress: () => void;
};
