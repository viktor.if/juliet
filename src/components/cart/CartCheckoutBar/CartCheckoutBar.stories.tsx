import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';
import React from 'react';

import CartCheckoutBar from './CartCheckoutBar';
import CenterView from '../../decorators/CenterView';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StyleSheet } from 'react-native';
import { TCartCheckoutBarProp } from './types';

const defaultProps: TCartCheckoutBarProp = {
  onPressCheckout: action('onPressCheckout'),
  total: '9.99',
};

storiesOf('Cart / Checkout bar', module)
  .addDecorator((getStory) => (
    <SafeAreaProvider style={styles.safeArea}>
      <CenterView>{getStory()}</CenterView>
    </SafeAreaProvider>
  ))
  .add('no total', () => <CartCheckoutBar {...defaultProps} total="0.00" />)
  .add('with total', () => <CartCheckoutBar {...defaultProps} />);

const styles = StyleSheet.create({
  safeArea: {
    minWidth: 380,
  },
});
