import React from 'react';
import { View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import ButtonRounded from '../../buttons/ButtonRounded';
import InfoOutlineIcon from '../../../../assets/svgs/information-outline.svg';
import styles from './styles';
import { TCartCheckoutBarProp } from './types';

export default ({ total, onPressCheckout }: TCartCheckoutBarProp) => {
  const safeInsets = useSafeAreaInsets();

  return (
    <View style={[styles.container, { paddingBottom: safeInsets.bottom }]}>
      <View style={styles.infoContainer}>
        <InfoOutlineIcon fill="#9797a8" />
        <Text style={styles.totalText}>
          <Text style={styles.totalBoldText}>Total: </Text>${total}
        </Text>
      </View>
      <ButtonRounded
        disabled={+total === 0}
        onPress={onPressCheckout}
        text="Checkout"
        uppercased
        testID="cart-checkout-bar-checkout-button"
      />
    </View>
  );
};
