import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    bottom: 0,
    elevation: 10,
    flexDirection: 'row',
    left: 0,
    minHeight: 62,
    opacity: 0.85,
    paddingLeft: 20,
    paddingRight: 16,
    paddingVertical: 2,
    position: 'absolute',
    right: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
  },
  infoContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    flexGrow: 1,
  },
  totalText: {
    color: '#28284b',
    fontSize: RFValue(18, BASE_SCREEN_HEIGHT),
    fontWeight: '400',
    lineHeight: RFValue(22, BASE_SCREEN_HEIGHT),
    marginHorizontal: 10,
  },
  totalBoldText: {
    color: '#28284b',
    fontWeight: '700',
  },
});
