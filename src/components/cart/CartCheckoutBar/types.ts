export type TCartCheckoutBarProp = {
  onPressCheckout: () => void;
  total: string;
};
