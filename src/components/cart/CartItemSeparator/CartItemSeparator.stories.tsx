import { storiesOf } from '@storybook/react-native';
import React from 'react';

import CartItemSeparator from './CartItemSeparator';
import CenterView from '../../decorators/CenterView';

storiesOf('Cart / Item separator', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('default', () => <CartItemSeparator />);
