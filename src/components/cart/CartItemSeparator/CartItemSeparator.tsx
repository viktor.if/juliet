import React, { useRef } from 'react';
import { Text, Animated } from 'react-native';

import styles from './styles';
import { TCartItemSeparatorProp } from './types';
import { useMount } from '../../../utils/hooks';

export default ({ containerStyle }: TCartItemSeparatorProp) => {
  const opacityAnimated = useRef(new Animated.Value(0));

  const fadeIn = () => {
    Animated.timing(opacityAnimated.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 350,
    }).start();
  };

  useMount(() => fadeIn());

  return (
    <Animated.View
      style={[
        styles.container,
        containerStyle,
        { opacity: opacityAnimated.current },
      ]}
    >
      <Text style={styles.separatorText}>+</Text>
    </Animated.View>
  );
};
