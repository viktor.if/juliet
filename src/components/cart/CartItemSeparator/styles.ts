import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: Layout.baseMarging,
    justifyContent: 'flex-end',
    paddingHorizontal: Layout.baseMarging,
    width: Metrics.screenWidth,
  },
  separatorText: {
    color: '#dfdfe6',
    fontSize: RFValue(24, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(26, BASE_SCREEN_HEIGHT),
  },
});
