import { StyleProp, ViewStyle } from 'react-native';

export type TCartItemSeparatorProp = {
  containerStyle?: StyleProp<ViewStyle>;
};
