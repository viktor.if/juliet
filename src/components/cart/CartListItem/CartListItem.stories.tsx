import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import CartListItem from './CartListItem';
import CenterView from '../../decorators/CenterView';
import { TCartListItemProp } from './types';

const defaultProps: TCartListItemProp = {
  image: '',
  name: 'Product name',
  qtyInCart: 0,
  onAddToCart: action('onAddToCart'),
  onPressImage: action('onPressImage'),
  onRemoveFromCart: action('onRemoveFromCart'),
  price: 9.99,
};

storiesOf('Cart /  List item', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('not added to cart', () => (
    <CartListItem {...defaultProps} qtyInCart={9} />
  ))
  .add('added to cart', () => <CartListItem {...defaultProps} />);
