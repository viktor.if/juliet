import React, { useRef } from 'react';
import FastImage from 'react-native-fast-image';
import { View, Text, Animated, TouchableOpacity } from 'react-native';

import ButtonRounded from '../../buttons/ButtonRounded';
import styles from './styles';
import { TCartListItemProp } from './types';
import { useMount } from '../../../utils/hooks';

export default ({
  containerStyle,
  id,
  image,
  name,
  onAddToCart,
  onPressImage,
  onRemoveFromCart,
  price,
  qtyInCart,
}: TCartListItemProp) => {
  const opacityAnimated = useRef(new Animated.Value(0));

  const total = (qtyInCart * price).toFixed(2);

  const fadeIn = () => {
    Animated.timing(opacityAnimated.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 350,
    }).start();
  };

  useMount(() => fadeIn());

  return (
    <Animated.View
      style={[
        styles.container,
        containerStyle,
        { opacity: opacityAnimated.current },
      ]}
    >
      {/** Card */}
      <View style={styles.card}>
        {/** Image */}
        <TouchableOpacity onPress={onPressImage}>
          <FastImage
            source={{ uri: image }}
            resizeMode={FastImage.resizeMode.cover}
            style={styles.image}
          />
        </TouchableOpacity>

        {/** Product info */}
        <View style={styles.infoContainer}>
          <Text style={styles.nameText} numberOfLines={2}>
            {name.trim()}
          </Text>
          <Text style={styles.priceText}>
            <Text style={styles.priceTextLight}>$</Text>
            {price.toFixed(2)} / kg
          </Text>

          {/** Buttons */}
          <View style={styles.buttonContainer}>
            {qtyInCart === 0 ? (
              <ButtonRounded
                onPress={() => onAddToCart(1)}
                small
                text="add"
                uppercased
                testID={`cart-list-item-add-button-${id}`}
              />
            ) : (
              <>
                <ButtonRounded
                  innerContainerStyle={styles.smallButton}
                  onPress={() => onRemoveFromCart(1)}
                  pale
                  small
                  text="  -  "
                  textStyle={styles.smallButtonText}
                  uppercased
                  testID={`cart-list-item-minus-button-${id}`}
                />
                <Text style={styles.numInCartText} numberOfLines={1}>
                  {qtyInCart}
                </Text>
                <ButtonRounded
                  innerContainerStyle={styles.smallButton}
                  onPress={() => onAddToCart(1)}
                  pale
                  small
                  text="  +  "
                  textStyle={styles.smallButtonText}
                  uppercased
                  testID={`cart-list-item-plus-button-${id}`}
                />
              </>
            )}
          </View>
        </View>
      </View>

      {/** Total */}
      <View style={styles.totalContainer}>
        <View style={styles.totalContainerTextContainer}>
          <Text style={styles.totalContainerText}>${total}</Text>
        </View>
      </View>
    </Animated.View>
  );
};
