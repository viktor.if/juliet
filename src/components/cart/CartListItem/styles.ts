import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: Layout.baseMarging,
    width: Metrics.screenWidth,
  },

  /** Card */
  card: {
    backgroundColor: '#fafafc',
    borderBottomWidth: 2 * Layout.baseBorderWidth,
    borderColor: '#eaeaee',
    borderRadius: Layout.baseBorderRadius,
    borderWidth: Layout.baseBorderWidth,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'hidden',
  },
  image: {
    backgroundColor: '#eaeaee',
    borderBottomLeftRadius: Layout.baseBorderRadius,
    borderBottomRightRadius: Layout.baseBorderRadius,
    borderTopLeftRadius: Layout.baseBorderRadius,
    borderTopRightRadius: Layout.baseBorderRadius,
    minHeight: 0.28 * Metrics.screenWidth,
    overflow: 'hidden',
    width: 0.28 * Metrics.screenWidth,
  },

  /** Details */
  infoContainer: {
    flex: 1,
    marginVertical: 8,
  },
  nameText: {
    color: '#2c2c4c',
    fontSize: RFValue(15, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(15, BASE_SCREEN_HEIGHT) + 4,
    paddingHorizontal: 12,
  },
  priceText: {
    color: '#a8a8b2',
    flex: 1,
    flexDirection: 'row',
    fontSize: RFValue(14, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(14, BASE_SCREEN_HEIGHT) + 4,
    paddingHorizontal: 12,
  },
  priceTextLight: {
    fontWeight: '400',
  },

  /** Total */
  totalContainer: {
    minWidth: '20%',
  },
  totalContainerTextContainer: {
    alignItems: 'flex-end',
    flexGrow: 1,
    justifyContent: 'center',
    width: '100%',
  },
  totalContainerText: {
    color: '#333359',
    fontSize: RFValue(20, BASE_SCREEN_HEIGHT),
    fontWeight: '400',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT) + 4,
    paddingLeft: 8,
    textAlign: 'right',
  },

  /** Buttons */
  buttonContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginHorizontal: 6,
    paddingTop: 4,
  },
  smallButton: {
    paddingVertical: 4,
  },
  smallButtonText: {
    fontSize: RFValue(18, BASE_SCREEN_HEIGHT),
    lineHeight: RFValue(22, BASE_SCREEN_HEIGHT),
    fontWeight: '700',
  },
  numInCartText: {
    color: '#000',
    flex: 1,
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT),
    marginHorizontal: 2,
    textAlign: 'center',
  },
});
