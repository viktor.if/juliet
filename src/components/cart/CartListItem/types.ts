import { StyleProp, ViewStyle } from 'react-native';

export type TCartListItemProp = {
  containerStyle?: StyleProp<ViewStyle>;
  id: number;
  image: string;
  name: string;
  onAddToCart: (num: number) => void;
  onPressImage: () => void;
  onRemoveFromCart: (num: number) => void;
  price: number;
  qtyInCart: number;
};
