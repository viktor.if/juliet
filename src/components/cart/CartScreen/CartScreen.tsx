import React from 'react';
import _ from 'lodash';
import { FlatList, ListRenderItem, StyleProp, ViewStyle } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

// Components
import CartListItem from '../CartListItem';
import CartItemSeparator from '../CartItemSeparator';
import ProductEmptyList from '../../products/ProductEmptyList';
import CartCheckoutBar from '../CartCheckoutBar';

import styles from './styles';
import useController from './useController';
import { EAppStackPages } from '../../../navigation/routes';
import { TCartScreenProp } from './types';
import { TProduct } from '../../../duck/productAll/types';

export default (props: TCartScreenProp) => {
  const { cart, navigation, addToCart, removeFromCart } = props;

  const { productsInCart, contentContainerStyle, totalNum, onPressCheckout } =
    useController(props);

  /** Render item */
  const renderItem: ListRenderItem<TProduct> = ({ item, index }) => {
    const qtyInCart = cart[item.id] ? cart[item.id].qty : 0;

    const containerStyle: StyleProp<ViewStyle> =
      index + 1 === productsInCart.length ? styles.lastListItem : undefined;

    const onPressImage = () => {
      navigation.navigate(EAppStackPages.ProductDetailsScreen, {
        productId: item.id,
      });
    };

    const onAddToCart = () => addToCart(item.id, 1);
    const onRemoveFromCart = () => removeFromCart(item.id, 1);

    return (
      <CartListItem
        containerStyle={containerStyle}
        id={item.id}
        image={item.image}
        name={item.name}
        onAddToCart={onAddToCart}
        onPressImage={onPressImage}
        onRemoveFromCart={onRemoveFromCart}
        price={item.price}
        qtyInCart={qtyInCart}
      />
    );
  };

  return (
    <SafeAreaView
      style={[styles.screen]}
      edges={['left', 'right']}
      testID="cart-screen"
    >
      <FlatList
        ItemSeparatorComponent={CartItemSeparator}
        ListEmptyComponent={
          <ProductEmptyList containerStyle={styles.listEmpty} />
        }
        contentContainerStyle={contentContainerStyle}
        data={productsInCart}
        initialNumToRender={10}
        keyExtractor={(item) => _.toString(item.id)}
        numColumns={1}
        renderItem={renderItem}
        style={styles.list}
      />

      <CartCheckoutBar total={totalNum} onPressCheckout={onPressCheckout} />
    </SafeAreaView>
  );
};
