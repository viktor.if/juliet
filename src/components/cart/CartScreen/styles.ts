import { StyleSheet } from 'react-native';
import { Metrics, Layout } from '../../../styles';

export default StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listContainer: {
    flexGrow: 1,
    paddingTop: Layout.baseMarging,
    width: Metrics.screenWidth,
  },
  list: {},
  lastListItem: {
    marginBottom: 50 + Layout.baseMarging,
  },
  listEmpty: {
    marginBottom: 50 + Layout.baseMarging,
  },
});
