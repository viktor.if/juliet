import { StackNavigationProp } from '@react-navigation/stack';

import { EAppStackPages } from '../../../navigation/routes';
import { TAppStackParamList } from '../../../navigation/types';
import { TState as TCartState } from '../../../duck/cart/types';
import { TState as TProductsAllState } from '../../../duck/productAll/types';
import { EdgeInsets } from 'react-native-safe-area-context';

export type TScreenNavigationProp = StackNavigationProp<
  TAppStackParamList,
  EAppStackPages.CartScreen
>;

export type TCartScreenProp = {
  addToCart: (prodId: number, qty: number) => void;
  cart: TCartState;
  navigation: TScreenNavigationProp;
  prodAllList: TProductsAllState['data'];
  removeFromCart: (prodId: number, qty: number) => void;
  safeInsets: EdgeInsets;
};
