import { Alert, StyleProp, ViewStyle } from 'react-native';

import styles from './styles';
import { TCartScreenProp } from './types';

export default ({ safeInsets, cart, prodAllList }: TCartScreenProp) => {
  const productsInCart = Object.values(cart).map(
    (item) => prodAllList.byId[item.id],
  );

  const contentContainerStyle: StyleProp<ViewStyle> = [
    styles.listContainer,
    { paddingBottom: safeInsets.bottom },
  ];

  const totalNum: string = Object.values(cart)
    .reduce((acc, curr) => acc + curr.qty * prodAllList.byId[curr.id].price, 0)
    .toFixed(2);

  const onPressCheckout = () =>
    Alert.alert('Checkout', 'This operation in not ready yet', [
      { text: 'OK' },
    ]);

  return {
    onPressCheckout,
    totalNum,
    contentContainerStyle,
    productsInCart,
  };
};
