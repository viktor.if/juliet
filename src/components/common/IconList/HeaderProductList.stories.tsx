import React from 'react';
import { storiesOf } from '@storybook/react-native';

import IconList from './index';
import TopView from '../../decorators/TopView';

storiesOf('Icons', module)
  .addDecorator((story) => <TopView>{story()}</TopView>)
  .add('default', () => <IconList />);
