import React, { lazy, Suspense } from 'react';
import { View, FlatList, Text } from 'react-native';

import styles from './styles';
import { TIcon } from './type';

const icons: TIcon[] = [
  {
    Component: lazy(
      () => import('../../../../assets/svgs/playlist-remove.svg'),
    ),
    name: 'playlist-remove',
  },
  {
    Component: lazy(
      () => import('../../../../assets/svgs/information-outline.svg'),
    ),
    name: 'information-outline',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/home.svg')),
    name: 'home',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/heart.svg')),
    name: 'heart',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/heart-outline.svg')),
    name: 'heart-outline',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/close.svg')),
    name: 'close',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/basket.svg')),
    name: 'basket',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/basket-outline.svg')),
    name: 'basket-outline',
  },
  {
    Component: lazy(() => import('../../../../assets/svgs/arrow-left.svg')),
    name: 'arrow-left',
  },
];

export default () => {
  return (
    <FlatList
      contentContainerStyle={styles.flatlist}
      data={icons}
      renderItem={({ item }) => {
        const { Component, name } = item;

        return (
          <Suspense fallback={null}>
            <View style={styles.iconContainer}>
              <Component fill="#000" stroke="#000" height={24} />
              <Text style={styles.iconName}>{name}</Text>
            </View>
          </Suspense>
        );
      }}
      numColumns={2}
    />
  );
};
