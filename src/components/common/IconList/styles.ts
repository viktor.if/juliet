import { StyleSheet } from 'react-native';
import { Metrics } from '../../../styles';

export default StyleSheet.create({
  flatlist: {
    flex: 1,
    width: Metrics.screenWidth,
  },
  iconContainer: {
    alignItems: 'center',
    padding: 16,
    width: Metrics.screenWidth / 2,
  },
  iconName: {
    color: '#000',
    marginTop: 4,
  },
});
