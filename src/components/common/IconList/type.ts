import { SvgProps } from 'react-native-svg';

export type TIcon = {
  Component: React.LazyExoticComponent<React.FC<SvgProps>>;
  name: string;
};
