import React from 'react';
import { View } from 'react-native';

import styles from './style';
import { TCenterView } from './types';

export default ({ children }: TCenterView) => (
  <View style={styles.main}>{children}</View>
);
