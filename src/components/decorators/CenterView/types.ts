export type TCenterView = {
  children: React.ReactNode;
};
