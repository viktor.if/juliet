import React from 'react';
import { View } from 'react-native';
import style from './style';
import { TTopView } from './types';

export default ({ children }: TTopView) => (
  <View style={style.main}>{children}</View>
);
