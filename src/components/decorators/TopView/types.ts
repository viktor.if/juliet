export type TTopView = {
  children: React.ReactNode;
};
