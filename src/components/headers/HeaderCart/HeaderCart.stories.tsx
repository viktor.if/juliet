import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';
import { StyleSheet } from 'react-native';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import HeaderCart from './HeaderCart';
import TopView from '../../decorators/TopView';
import { THeaderCartProp } from './types';

const defaultProps: THeaderCartProp = {
  onPressClose: action('onPressClose'),
  onPressHome: action('onPressHome'),
  title: 'Cart header',
};

storiesOf('Headers / Cart', module)
  .addDecorator((getStory) => (
    <SafeAreaProvider style={styles.safeArea}>
      <TopView>{getStory()}</TopView>
    </SafeAreaProvider>
  ))
  .add('default', () => <HeaderCart {...defaultProps} />);

const styles = StyleSheet.create({
  safeArea: {
    minWidth: 380,
  },
});
