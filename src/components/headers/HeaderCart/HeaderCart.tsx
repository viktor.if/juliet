import React from 'react';
import { View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import styles from './styles';
import { THeaderCartProp } from './types';
import HeaderHomeButton from '../../buttons/HeaderHomeButton';
import HeaderArrowLeftButton from '../../buttons/HeaderArrowLeftButton';

export default ({ title, onPressClose, onPressHome }: THeaderCartProp) => {
  const insets = useSafeAreaInsets();

  return (
    <View style={[styles.container, { paddingTop: insets.top }]}>
      <HeaderArrowLeftButton onPress={onPressClose} />
      <Text style={styles.titleText}>{title}</Text>
      <HeaderHomeButton onPress={onPressHome} />
    </View>
  );
};
