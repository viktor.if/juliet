export type THeaderCartProp = {
  onPressClose: () => void;
  onPressHome: () => void;
  title: string;
};
