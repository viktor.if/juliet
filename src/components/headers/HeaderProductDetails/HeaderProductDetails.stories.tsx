import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StyleSheet } from 'react-native';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import HeaderProductDetails from './HeaderProductDetails';
import TopView from '../../decorators/TopView';
import { THeaderProductDetailsProp } from './types';

const defaultProps: THeaderProductDetailsProp = {
  onPressCart: action('onPressCart'),
  onPressClose: action('onPressClose'),
  qtyProdsInCart: 0,
  title: 'Product details',
};

storiesOf('Headers / Product details', module)
  .addDecorator((getStory) => (
    <SafeAreaProvider style={styles.safeArea}>
      <TopView>{getStory()}</TopView>
    </SafeAreaProvider>
  ))
  .add('not added to cart', () => <HeaderProductDetails {...defaultProps} />)
  .add('added to cart', () => (
    <HeaderProductDetails {...defaultProps} qtyProdsInCart={1} />
  ))
  .add('pending', () => <HeaderProductDetails {...defaultProps} pending />);

const styles = StyleSheet.create({
  safeArea: {
    minWidth: 380,
  },
});
