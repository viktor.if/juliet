import React from 'react';
import { View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import styles from './styles';
import { THeaderProductDetailsProp } from './types';
import HeaderCloseButton from '../../buttons/HeaderCloseButton';
import HeaderCartButton from '../../buttons/HeaderCartButton';

export default ({
  onPressCart,
  onPressClose,
  pending,
  qtyProdsInCart,
  title,
}: THeaderProductDetailsProp) => {
  const insets = useSafeAreaInsets();

  return (
    <View style={[styles.container, { paddingTop: insets.top }]}>
      <HeaderCloseButton onPress={onPressClose} />
      <Text style={styles.titleText}>{title}</Text>
      <HeaderCartButton
        disabled={pending}
        onPress={onPressCart}
        qtyProdsInCart={qtyProdsInCart}
      />
    </View>
  );
};
