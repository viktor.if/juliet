import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    elevation: 2,
    flexDirection: 'row',
    minHeight: 32,
    width: Metrics.screenWidth,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
  },
  titleText: {
    color: '#000',
    flex: 1,
    flexDirection: 'row',
    fontSize: RFValue(20, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(24, BASE_SCREEN_HEIGHT),
    marginLeft: 8,
  },
});
