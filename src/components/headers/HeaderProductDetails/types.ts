export type THeaderProductDetailsProp = {
  onPressCart: () => void;
  onPressClose: () => void;
  pending?: boolean;
  qtyProdsInCart: number;
  title: string;
};
