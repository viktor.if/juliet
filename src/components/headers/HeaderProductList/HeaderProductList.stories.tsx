import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StyleSheet } from 'react-native';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import HeaderProductList from './HeaderProductList';
import TopView from '../../decorators/TopView';
import { THeaderProductListProp } from './types';

const defaultProps: THeaderProductListProp = {
  favToggled: false,
  onChangeText: action('onChangeText'),
  onClearSearchText: action('onClearSearchText'),
  onPressCart: action('onPressCart'),
  onPressFav: action('onPressFav'),
  pending: false,
  placeholder: 'Placeholder',
  qtyProdsInCart: 0,
  searchValue: '',
};

storiesOf('Headers / Product list', module)
  .addDecorator((getStory) => (
    <SafeAreaProvider style={styles.safeArea}>
      <TopView>{getStory()}</TopView>
    </SafeAreaProvider>
  ))
  .add('cart empty', () => <HeaderProductList {...defaultProps} />)
  .add('cart filled', () => (
    <HeaderProductList {...defaultProps} qtyProdsInCart={1} />
  ))
  .add('with input text', () => (
    <HeaderProductList {...defaultProps} searchValue="Product" />
  ))
  .add('pending', () => <HeaderProductList {...defaultProps} pending />)
  .add('favorites on', () => (
    <HeaderProductList {...defaultProps} favToggled />
  ));

const styles = StyleSheet.create({
  safeArea: {
    minWidth: 380,
  },
});
