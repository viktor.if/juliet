import React from 'react';
import { View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import HeaderFavoritesButton from '../../buttons/HeaderFavoritesButton';
import HeaderSearchBar from '../../inputs/HeaderSearchBar';
import HeaderCartButton from '../../buttons/HeaderCartButton';

import styles from './styles';
import { THeaderProductListProp } from './types';

export default ({
  favToggled,
  onChangeText,
  onClearSearchText,
  onPressFav,
  onPressCart,
  pending,
  placeholder,
  qtyProdsInCart,
  searchValue,
}: THeaderProductListProp) => {
  const insets = useSafeAreaInsets();

  return (
    <View style={[styles.container, { marginTop: insets.top }]}>
      <HeaderFavoritesButton
        disabled={pending}
        onPress={onPressFav}
        toggled={favToggled}
      />
      <HeaderSearchBar
        onChangeText={onChangeText}
        onClearSearchText={onClearSearchText}
        pending={pending}
        placeholder={placeholder}
        value={searchValue}
      />
      <HeaderCartButton
        disabled={pending}
        onPress={onPressCart}
        qtyProdsInCart={qtyProdsInCart}
      />
    </View>
  );
};
