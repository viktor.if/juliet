import { StyleSheet } from 'react-native';
import { Metrics } from '../../../styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 32,
    width: Metrics.screenWidth,
  },
});
