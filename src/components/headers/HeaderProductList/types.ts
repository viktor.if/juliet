export type THeaderProductListProp = {
  favToggled: boolean;
  onChangeText: (text: string) => void;
  onClearSearchText: () => void;
  onPressFav: () => void;
  onPressCart: () => void;
  pending?: boolean;
  placeholder: string;
  qtyProdsInCart: number;
  searchValue: string;
};
