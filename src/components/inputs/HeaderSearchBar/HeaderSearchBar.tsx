import React, { useState } from 'react';

import {
  ActivityIndicator,
  StyleProp,
  TextInput,
  TextStyle,
  TouchableOpacity,
  View,
} from 'react-native';

import { THeaderSearchBarProp } from './types';
import styles from './styles';
import CloseIcon from '../../../../assets/svgs/close.svg';

export default ({
  onChangeText,
  onClearSearchText,
  pending,
  placeholder,
  value,
}: THeaderSearchBarProp) => {
  const [focused, setFocused] = useState<boolean>(false);

  const style: StyleProp<TextStyle> = [
    styles.input,
    focused ? styles.containerFocused : undefined,
  ];

  return (
    <View style={styles.container}>
      <TextInput
        onBlur={() => setFocused(false)}
        onChangeText={onChangeText}
        onFocus={() => setFocused(true)}
        placeholder={placeholder}
        placeholderTextColor="#b5b6b8"
        style={style}
        value={value}
        testID="search-bar-text-input"
      />

      {/** Close button */}
      {!!value && !pending && (
        <TouchableOpacity
          onPress={onClearSearchText}
          style={styles.closeButton}
        >
          <CloseIcon fill="#b5b6b8" />
        </TouchableOpacity>
      )}

      {/** Pending indicator */}
      {pending && focused && (
        <ActivityIndicator
          color="#b5b6b8"
          size="small"
          style={styles.pendingIndicator}
        />
      )}
    </View>
  );
};
