import { Platform, StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: 0,
    marginVertical: 8,
  },
  input: {
    backgroundColor: '#f1f2f5',
    borderRadius: 8,
    color: '#2c2c4c',
    flex: 1,
    flexDirection: 'row',
    fontSize: RFValue(14, BASE_SCREEN_HEIGHT),
    lineHeight: RFValue(18, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    paddingLeft: 16,
    paddingRight: 44,
    paddingVertical: Platform.OS === 'android' ? 8 : 12,
  },
  containerFocused: {
    backgroundColor: '#fff',
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
  },
  closeButton: {
    bottom: 0,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 6,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  pendingIndicator: {
    bottom: 0,
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingVertical: 8,
    position: 'absolute',
    right: 0,
    top: 0,
  },
});
