export type THeaderSearchBarProp = {
  onChangeText: (text: string) => void;
  onClearSearchText: () => void;
  pending?: boolean;
  placeholder: string;
  value: string;
};
