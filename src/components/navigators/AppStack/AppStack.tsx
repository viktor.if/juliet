import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'react-native';

import { EAppStackPages } from '../../../navigation/routes';
import { TAppStackParamList } from '../../../navigation/types';
import { theme } from '../../../navigation/theme';
import { TAppStackProp } from './types';

import CartScreenContainer from '../../../containers/cart/CartScreenContainer';
import HeaderCart from '../../headers/HeaderCart';
import HeaderProductList from '../../headers/HeaderProductList';
import ProductDetailsScreenContainer from '../../../containers/products/ProductDetailsScreenContainer';
import ProductListScreenContainer from '../../../containers/products/ProductListScreenContainer';
import HeaderProductDetails from '../../headers/HeaderProductDetails';

const Stack = createStackNavigator<TAppStackParamList>();

export default ({ cart, prodAllPending }: TAppStackProp) => {
  const qtyProdsInCart = Object.values(cart).reduce(
    (acc, currValue) => acc + currValue.qty,
    0,
  );

  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <NavigationContainer {...{ theme }}>
        <Stack.Navigator initialRouteName={EAppStackPages.ProductListScreen}>
          {/** Product list */}
          <Stack.Screen
            component={ProductListScreenContainer}
            name={EAppStackPages.ProductListScreen}
            options={({ route, navigation }) => ({
              presentation: 'card',
              header: () => (
                <HeaderProductList
                  favToggled={route.params?.favProdShown ?? false}
                  qtyProdsInCart={qtyProdsInCart}
                  onChangeText={route.params?.onChangeSearchText}
                  onClearSearchText={route.params?.onClearSearchText}
                  onPressFav={
                    route.params?.toggleFavProdVisibility ?? undefined
                  }
                  onPressCart={() =>
                    navigation.navigate(EAppStackPages.CartScreen)
                  }
                  pending={prodAllPending}
                  placeholder="Search Groceries or Products"
                  searchValue={route.params?.searchText}
                />
              ),
            })}
          />

          {/** Product details */}
          <Stack.Screen
            component={ProductDetailsScreenContainer}
            name={EAppStackPages.ProductDetailsScreen}
            options={({ navigation }) => ({
              presentation: 'card',
              header: () => (
                <HeaderProductDetails
                  onPressClose={() => navigation.goBack()}
                  pending={prodAllPending}
                  qtyProdsInCart={qtyProdsInCart}
                  title="Fruits & Vegetables"
                  onPressCart={() =>
                    navigation.navigate(EAppStackPages.CartScreen)
                  }
                />
              ),
            })}
          />

          {/** Cart */}
          <Stack.Screen
            component={CartScreenContainer}
            name={EAppStackPages.CartScreen}
            options={({ navigation }) => ({
              presentation: 'card',
              header: () => (
                <HeaderCart
                  onPressClose={() => navigation.goBack()}
                  title="My Cart"
                  onPressHome={() => {
                    navigation.navigate(EAppStackPages.ProductListScreen);
                  }}
                />
              ),
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};
