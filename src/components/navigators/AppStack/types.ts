import { TState as TCartState } from '../../../duck/cart/types';

export type TAppStackProp = {
  cart: TCartState;
  prodAllPending: boolean;
};
