import React from 'react';
import { View, Text } from 'react-native';

import ButtonRounded from '../../buttons/ButtonRounded';
import styles from './styles';
import { TProductDetailsDashboardProp } from './types';

export default ({
  containerStyle,
  onAddToCart,
  onRemoveFromCart,
  qtyInCart,
}: TProductDetailsDashboardProp) => {
  return (
    <View style={[styles.container, containerStyle]}>
      {/** Add button */}
      <View style={qtyInCart > 0 && styles.hidden}>
        <ButtonRounded
          text="Add"
          uppercased
          onPress={onAddToCart}
          testID="product-details-add-button"
        />
      </View>

      {/** Small buttons */}
      <View
        style={[
          styles.plusMinusButtonContainer,
          qtyInCart === 0 && styles.hidden,
        ]}
        pointerEvents={qtyInCart > 0 ? 'auto' : 'none'}
      >
        <ButtonRounded
          innerContainerStyle={styles.smallButton}
          onPress={onRemoveFromCart}
          pale
          small
          text="  -  "
          textStyle={styles.smallButtonText}
          uppercased
          testID="product-details-minus-button"
        />
        <Text style={styles.numInCartText} numberOfLines={1}>
          {qtyInCart}
        </Text>
        <ButtonRounded
          innerContainerStyle={styles.smallButton}
          onPress={onAddToCart}
          pale
          small
          text="  +  "
          textStyle={styles.smallButtonText}
          uppercased
          testID="product-details-plus-button"
        />
      </View>
    </View>
  );
};
