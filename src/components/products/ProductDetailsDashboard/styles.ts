import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fafafc',
    borderBottomWidth: 2 * Layout.baseBorderWidth,
    borderColor: '#eaeaee',
    borderRadius: Layout.baseBorderRadius,
    borderWidth: Layout.baseBorderWidth,
    justifyContent: 'center',
    marginBottom: Layout.baseMarging,
    overflow: 'hidden',
    paddingVertical: 8,
    width: Metrics.screenWidth - 2 * Layout.baseMarging,
    marginLeft: Layout.baseMarging,
    marginRight: Layout.baseMarging,
    flexDirection: 'row',
  },
  plusMinusButtonContainer: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  smallButton: {
    paddingVertical: 4,
  },
  smallButtonText: {
    fontSize: RFValue(18, BASE_SCREEN_HEIGHT),
    fontWeight: '700',
    lineHeight: RFValue(22, BASE_SCREEN_HEIGHT),
  },
  numInCartText: {
    color: '#000',
    paddingHorizontal: 12,
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT),
    marginHorizontal: 2,
    textAlign: 'center',
  },
  hidden: {
    opacity: 0,
  },
});
