import { StyleProp, ViewStyle } from 'react-native';

export type TProductDetailsDashboardProp = {
  containerStyle?: StyleProp<ViewStyle>;
  onAddToCart: () => void;
  onRemoveFromCart: () => void;
  qtyInCart: number;
};
