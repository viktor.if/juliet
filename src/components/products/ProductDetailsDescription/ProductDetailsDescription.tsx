import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import { TProductDetailsDescriptionProp } from './types';

export default ({ text }: TProductDetailsDescriptionProp) => {
  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Description</Text>
      <Text style={styles.prodocutDescriptionText}>{text}</Text>
    </View>
  );
};
