import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    paddingBottom: Layout.baseMarging,
    paddingHorizontal: Layout.baseMarging,
    width: Metrics.screenWidth,
  },
  titleText: {
    color: '#000',
    fontSize: RFValue(18, BASE_SCREEN_HEIGHT),
    fontWeight: '600',
    lineHeight: RFValue(22, BASE_SCREEN_HEIGHT),
    paddingBottom: 12,
  },
  prodocutDescriptionText: {
    color: '#59597d',
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '400',
    lineHeight: RFValue(24, BASE_SCREEN_HEIGHT),
  },
});
