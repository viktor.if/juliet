import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import FastImage from 'react-native-fast-image';

import { TProductDetailsImageProp } from './types';
import styles from './styles';
import HeartIcon from '../../../../assets/svgs/heart.svg';
import HeartOutlineIcon from '../../../../assets/svgs/heart-outline.svg';

export default ({
  containerStyle,
  favorite,
  image,
  onAddToFavorites,
  onRemoveFromFavorites,
}: TProductDetailsImageProp) => {
  const onPressFavoriteButton = () => {
    if (favorite) {
      onRemoveFromFavorites();
    } else {
      onAddToFavorites();
    }
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <FastImage
        source={{ uri: image }}
        resizeMode={FastImage.resizeMode.cover}
        style={styles.image}
      />

      {/** Favorite */}
      <TouchableOpacity
        style={styles.favoriteButton}
        onPress={onPressFavoriteButton}
      >
        {favorite ? (
          <HeartIcon fill="#ff2b4d" height={24} width={24} />
        ) : (
          <HeartOutlineIcon fill="#adadbe" height={24} width={24} />
        )}
      </TouchableOpacity>
    </View>
  );
};
