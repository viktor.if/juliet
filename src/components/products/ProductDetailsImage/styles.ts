import { StyleSheet } from 'react-native';

import { Metrics, Layout } from '../../../styles';

export default StyleSheet.create({
  container: {
    backgroundColor: '#fafafc',
    borderBottomWidth: 2 * Layout.baseBorderWidth,
    borderColor: '#eaeaee',
    borderRadius: Layout.baseBorderRadius,
    borderWidth: Layout.baseBorderWidth,
    height: Metrics.screenWidth - 2 * Layout.baseMarging,
    marginHorizontal: Layout.baseMarging,
    marginVertical: Layout.baseMarging,
    overflow: 'hidden',
    width: Metrics.screenWidth - 2 * Layout.baseMarging,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  favoriteButton: {
    alignSelf: 'center',
    left: 0,
    top: 0,
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 16,
    paddingBottom: 16,
    position: 'absolute',
  },
});
