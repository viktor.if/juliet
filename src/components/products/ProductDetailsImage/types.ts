import { StyleProp, ViewStyle } from 'react-native';

export type TProductDetailsImageProp = {
  containerStyle?: StyleProp<ViewStyle>;
  favorite: boolean;
  image: string;
  onAddToFavorites: () => void;
  onRemoveFromFavorites: () => void;
};
