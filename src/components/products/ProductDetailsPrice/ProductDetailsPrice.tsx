import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import { TProductDetailsPriceProp } from './types';

export default ({ pricePerItem, productName }: TProductDetailsPriceProp) => {
  const priceNum = pricePerItem.toFixed(2);

  return (
    <View style={styles.container}>
      <Text style={styles.productNameText}>{productName}</Text>
      <Text style={styles.productPriceText}>
        $<Text style={styles.productPriceTextBold}>{priceNum}</Text> / kg
      </Text>
    </View>
  );
};
