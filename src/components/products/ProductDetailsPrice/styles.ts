import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    flexDirection: 'row',
    paddingBottom: Layout.baseMarging,
    paddingHorizontal: 1.5 * Layout.baseMarging,
    width: Metrics.screenWidth,
  },
  productNameText: {
    color: '#333359',
    flex: 1,
    flexDirection: 'row',
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT),
  },
  productPriceText: {
    color: '#adadbe',
    flexDirection: 'row',
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '400',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT),
  },
  productPriceTextBold: {
    fontWeight: '500',
  },
});
