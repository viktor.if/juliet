export type TProductDetailsPriceProp = {
  productName: string;
  pricePerItem: number;
};
