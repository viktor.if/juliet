import { storiesOf } from '@storybook/react-native';
import React from 'react';

import CenterView from '../../decorators/CenterView';
import ProductDetailsScreen from './ProductDetailsScreen';
import { EAppStackPages } from '../../../navigation/routes';
import { TProductDetailsScreenProp } from './types';

const defaultProps: TProductDetailsScreenProp = {
  route: {
    name: EAppStackPages.ProductDetailsScreen,
    key: 'key',
    params: {
      productId: 1,
    },
  },
  safeInsets: {
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
  },
  cart: {
    1: {
      id: 1,
      qty: 0,
    },
  },
  prodAllList: {
    allIds: [1],
    byId: {
      1: {
        id: 1,
        name: 'Product name',
        image: '',
        price: 9.99,
        in_stock: true,
        description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo viverra maecenas accumsan lacus vel facilisis volutpat est velit. Aliquam ultrices sagittis orci a scelerisque purus semper. Id interdum velit laoreet id. Vitae purus faucibus ornare suspendisse sed. Iaculis at erat pellentesque adipiscing. Sapien faucibus et molestie ac feugiat sed lectus vestibulum mattis. Dapibus ultrices in iaculis nunc sed augue lacus viverra vitae. Phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec. Phasellus egestas tellus rutrum tellus pellentesque eu. Vitae aliquet nec ullamcorper sit amet.',
        created_at: '',
      },
    },
  },
  prodFavList: [],
  addToCart: () => undefined,
  addToFavorites: () => undefined,
  removeFromCart: () => undefined,
  removeFromFavorites: () => {},
};

storiesOf('Products / Details screen', module)
  .addDecorator((story) => <CenterView>{story()}</CenterView>)
  .add('not added to cart', () => <ProductDetailsScreen {...defaultProps} />)
  .add('added to cart', () => (
    <ProductDetailsScreen
      {...defaultProps}
      cart={{
        1: {
          id: 1,
          qty: 9,
        },
      }}
    />
  ))
  .add('favorite', () => (
    <ProductDetailsScreen {...defaultProps} prodFavList={[1]} />
  ));
