import React from 'react';
import { ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import styles from './styles';
import useController from './useController';
import { Layout } from '../../../styles';
import { TProductDetailsScreenProp } from './types';

// Components
import ProductDetailsImage from '../ProductDetailsImage';
import ProductDetailsDashboard from '../ProductDetailsDashboard';
import ProductDetailsDescription from '../ProductDetailsDescription';
import ProductDetailsPrice from '../ProductDetailsPrice';

export default (props: TProductDetailsScreenProp) => {
  const { safeInsets } = props;

  const {
    description,
    image,
    isFavorite,
    name,
    onAddToCart,
    onAddToFavorites,
    onRemoveFromCart,
    onRemoveFromFavorites,
    price,
    qty,
  } = useController(props);

  return (
    <SafeAreaView
      style={styles.screen}
      edges={['left', 'right']}
      testID="product-details"
    >
      <ScrollView
        contentContainerStyle={[
          styles.scrollContainer,
          { paddingBottom: Layout.baseMarging + safeInsets.bottom },
        ]}
        testID="product-details-scroll"
      >
        {/** Image */}
        <ProductDetailsImage
          favorite={isFavorite}
          image={image}
          onAddToFavorites={onAddToFavorites}
          onRemoveFromFavorites={onRemoveFromFavorites}
        />

        {/** Price */}
        <ProductDetailsPrice productName={name} pricePerItem={price} />

        {/** Dashboard */}
        <ProductDetailsDashboard
          qtyInCart={qty}
          onAddToCart={onAddToCart}
          onRemoveFromCart={onRemoveFromCart}
        />

        {/** Description */}
        <ProductDetailsDescription text={description} />
      </ScrollView>
    </SafeAreaView>
  );
};
