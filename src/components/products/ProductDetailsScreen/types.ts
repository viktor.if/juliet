import { RouteProp } from '@react-navigation/native';
import { EdgeInsets } from 'react-native-safe-area-context';

import { EAppStackPages } from '../../../navigation/routes';
import { TAppStackParamList } from '../../../navigation/types';
import { TState as TCartState } from '../../../duck/cart/types';
import { TState as TProductsAllState } from '../../../duck/productAll/types';

export type TScreenRouteProp = RouteProp<
  TAppStackParamList,
  EAppStackPages.ProductDetailsScreen
>;

export type TProductDetailsScreenProp = {
  addToCart: (prodId: number, qty: number) => void;
  addToFavorites: (prodId: number) => void;
  cart: TCartState;
  prodAllList: TProductsAllState['data'];
  prodFavList: number[];
  removeFromCart: (prodId: number, qty: number) => void;
  removeFromFavorites: (prodId: number) => void;
  route: TScreenRouteProp;
  safeInsets: EdgeInsets;
};
