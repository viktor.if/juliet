import { TProductDetailsScreenProp } from './types';

export default ({
  addToCart,
  addToFavorites,
  cart,
  prodAllList,
  prodFavList,
  removeFromCart,
  removeFromFavorites,
  route,
}: TProductDetailsScreenProp) => {
  const { productId } = route.params;
  const { byId } = prodAllList;
  const { qty = 0 } = cart?.[productId] ?? {};

  const { name, price, description } = byId[productId];
  const { image } = byId[productId];
  const isFavorite = !!prodFavList.find((item) => item === productId);

  const onAddToFavorites = () => addToFavorites(productId);
  const onRemoveFromFavorites = () => removeFromFavorites(productId);
  const onAddToCart = () => addToCart(productId, 1);
  const onRemoveFromCart = () => removeFromCart(productId, 1);

  return {
    description,
    image,
    isFavorite,
    name,
    onAddToCart,
    onAddToFavorites,
    onRemoveFromCart,
    onRemoveFromFavorites,
    price,
    qty,
  };
};
