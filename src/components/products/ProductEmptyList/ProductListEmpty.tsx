import React, { useRef } from 'react';
import { Text, Animated } from 'react-native';

import PlaylistRemoveIcon from '../../../../assets/svgs/playlist-remove.svg';
import styles from './styles';
import { useMount } from '../../../utils/hooks';
import { TProductEmptyListProp } from './types';

export default ({ containerStyle }: TProductEmptyListProp) => {
  const opacityAnimated = useRef(new Animated.Value(0));

  const fadeIn = () => {
    Animated.timing(opacityAnimated.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 150,
    }).start();
  };

  useMount(() => fadeIn());

  return (
    <Animated.View
      style={[
        styles.container,
        containerStyle,
        { opacity: opacityAnimated.current },
      ]}
      testID="product-list-empty"
    >
      <PlaylistRemoveIcon fill="#b2b2b2" height={32} width={32} />
      <Text style={styles.text}>No products</Text>
    </Animated.View>
  );
};
