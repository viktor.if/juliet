import { StyleProp, ViewStyle } from 'react-native';

export type TProductEmptyListProp = {
  containerStyle?: StyleProp<ViewStyle>;
};
