import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react-native';

import ProductListItem from './ProductListItem';
import CenterView from '../../decorators/CenterView';
import { TProductListItemProp } from './types';

const defaultProps: TProductListItemProp = {
  favorite: false,
  image: '',
  name: 'Product name',
  qtyInCart: 0,
  onAddToCart: action('onAddToCart'),
  onAddToFavorites: action('onAddToFavorites'),
  onPressImage: action('onPressImage'),
  onRemoveFromCart: action('onRemoveFromCart'),
  onRemoveFromFavorites: action('onRemoveFromFavorites'),
  price: 9.99,
};

storiesOf('Products / List item', module)
  .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
  .add('not added to cart', () => <ProductListItem {...defaultProps} />)
  .add('added to cart', () => (
    <ProductListItem {...defaultProps} qtyInCart={9} />
  ))
  .add('favorite', () => <ProductListItem {...defaultProps} favorite />);
