import React, { useRef } from 'react';
import FastImage from 'react-native-fast-image';
import { View, Text, Animated, TouchableOpacity } from 'react-native';

import ButtonRounded from '../../buttons/ButtonRounded';
import styles from './styles';
import { TProductListItemProp } from './types';
import { useMount } from '../../../utils/hooks';

// Icons
import HeartIcon from '../../../../assets/svgs/heart.svg';
import HeartOutlineIcon from '../../../../assets/svgs/heart-outline.svg';

export default ({
  containerStyle,
  favorite,
  id,
  image,
  name,
  onAddToCart,
  onAddToFavorites,
  onPressImage,
  onRemoveFromCart,
  onRemoveFromFavorites,
  price,
  qtyInCart,
}: TProductListItemProp) => {
  const opacityAnimated = useRef(new Animated.Value(0));

  const fadeIn = () => {
    Animated.timing(opacityAnimated.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 350,
    }).start();
  };

  const onPressFavoriteButton = () => {
    if (favorite) {
      onRemoveFromFavorites();
    } else {
      onAddToFavorites();
    }
  };

  useMount(() => fadeIn());

  return (
    <Animated.View
      style={[
        styles.container,
        containerStyle,
        { opacity: opacityAnimated.current },
      ]}
      accessibilityLabel="productListLable"
      testID={`product-list-item-${id}`}
    >
      {/** Image */}
      <TouchableOpacity onPress={onPressImage}>
        <FastImage
          source={{ uri: image }}
          resizeMode={FastImage.resizeMode.cover}
          style={styles.image}
          testID={`product-list-item-image-${id}`}
        />
      </TouchableOpacity>

      {/** Text */}
      <View style={styles.textContainer}>
        <Text style={styles.nameText} numberOfLines={2}>
          {name.trim()}
        </Text>
        <Text style={styles.priceText}>
          <Text style={styles.priceTextLight}>$</Text>
          {price.toFixed(2)}
        </Text>
      </View>

      {/** Button */}
      <View style={styles.innerButtonContainer}>
        {qtyInCart === 0 ? (
          <ButtonRounded
            onPress={() => onAddToCart(1)}
            small
            text="add"
            uppercased
            testID={`product-list-item-add-button-${id}`}
          />
        ) : (
          <>
            <ButtonRounded
              innerContainerStyle={styles.smallButton}
              onPress={() => onRemoveFromCart(1)}
              pale
              small
              text="  -  "
              textStyle={styles.smallButtonText}
              uppercased
              testID={`product-list-item-minus-button-${id}`}
            />
            <Text style={styles.numInCartText} numberOfLines={1}>
              {qtyInCart}
            </Text>
            <ButtonRounded
              innerContainerStyle={styles.smallButton}
              onPress={() => onAddToCart(1)}
              pale
              small
              text="  +  "
              textStyle={styles.smallButtonText}
              uppercased
              testID={`product-list-item-plus-button-${id}`}
            />
          </>
        )}
      </View>

      {/** Favorite */}
      <TouchableOpacity
        onPress={onPressFavoriteButton}
        style={styles.favoriteButton}
        testID={`product-list-item-favorite-${id}`}
      >
        {favorite ? (
          <HeartIcon fill="#ff2b4d" />
        ) : (
          <HeartOutlineIcon fill="#adadbe" />
        )}
      </TouchableOpacity>
    </Animated.View>
  );
};
