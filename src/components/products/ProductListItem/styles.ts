import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Metrics, Layout } from '../../../styles';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

const containerWidth = (Metrics.screenWidth - 3 * Layout.baseMarging) / 2;

export default StyleSheet.create({
  container: {
    backgroundColor: '#fafafc',
    borderBottomWidth: 2 * Layout.baseBorderWidth,
    borderColor: '#eaeaee',
    borderRadius: Layout.baseBorderRadius,
    borderWidth: Layout.baseBorderWidth,
    justifyContent: 'space-between',
    marginBottom: Layout.baseMarging,
    overflow: 'hidden',
    paddingBottom: 12,
    width: containerWidth,
  },
  /** Image */
  image: {
    backgroundColor: '#eaeaee',
    borderBottomLeftRadius: Layout.baseBorderRadius,
    borderBottomRightRadius: Layout.baseBorderRadius,
    borderTopLeftRadius: Layout.baseBorderRadius - 2,
    borderTopRightRadius: Layout.baseBorderRadius - 2,
    height: containerWidth - 2 * Layout.baseBorderWidth,
    width: containerWidth - 2 * Layout.baseBorderWidth,
    marginTop: 0,
    paddingTop: 0,
    overflow: 'hidden',
  },
  /** Text */
  textContainer: {
    flexDirection: 'row',
    flexGrow: 1,
    marginHorizontal: 8,
    marginTop: 6,
    marginBottom: 8,
  },
  nameText: {
    color: '#2c2c4c',
    flex: 1,
    flexDirection: 'row',
    fontSize: RFValue(13, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(17, BASE_SCREEN_HEIGHT),
  },
  priceText: {
    color: '#a8a8b2',
    fontSize: RFValue(13, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(17, BASE_SCREEN_HEIGHT),
    marginLeft: 8,
  },
  priceTextLight: {
    fontWeight: '400',
  },
  /** Buttons */
  innerButtonContainer: {
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginHorizontal: 6,
  },
  smallButton: {
    paddingVertical: 4,
  },
  smallButtonText: {
    fontSize: RFValue(18, BASE_SCREEN_HEIGHT),
    fontWeight: '700',
    lineHeight: RFValue(22, BASE_SCREEN_HEIGHT),
  },
  /** Product number */
  numInCartText: {
    color: '#000',
    flex: 1,
    fontSize: RFValue(16, BASE_SCREEN_HEIGHT),
    fontWeight: '500',
    lineHeight: RFValue(20, BASE_SCREEN_HEIGHT),
    marginHorizontal: 2,
    textAlign: 'center',
  },
  /** Favorite */
  favoriteButton: {
    alignSelf: 'center',
    left: 0,
    top: 0,
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 16,
    paddingBottom: 16,
    position: 'absolute',
  },
});
