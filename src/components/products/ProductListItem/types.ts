import { StyleProp, ViewStyle } from 'react-native';

export type TProductListItemProp = {
  containerStyle?: StyleProp<ViewStyle>;
  favorite: boolean;
  id: number;
  image: string;
  name: string;
  onAddToCart: (num: number) => void;
  onAddToFavorites: () => void;
  onPressImage: () => void;
  onRemoveFromCart: (num: number) => void;
  onRemoveFromFavorites: () => void;
  price: number;
  qtyInCart: number;
};
