import React, { useRef } from 'react';
import { Text, Animated } from 'react-native';

import styles from './styles';
import { useMount } from '../../../utils/hooks';

export default () => {
  const opacityAnimated = useRef(new Animated.Value(0));

  const fadeIn = () => {
    Animated.timing(opacityAnimated.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 150,
    }).start();
  };

  useMount(() => fadeIn());

  return (
    <Animated.View
      style={[styles.container, { opacity: opacityAnimated.current }]}
    >
      <Text style={styles.text}>Loading...</Text>
    </Animated.View>
  );
};
