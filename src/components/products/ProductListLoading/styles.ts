import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { BASE_SCREEN_HEIGHT } from '../../../utils/constants';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  text: {
    color: '#b2b2b2',
    fontWeight: '400',
    fontSize: RFValue(24, BASE_SCREEN_HEIGHT),
    lineHeight: RFValue(26, BASE_SCREEN_HEIGHT),
  },
});
