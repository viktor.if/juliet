import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useHeaderHeight } from '@react-navigation/elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import _ from 'lodash';

import {
  FlatList,
  KeyboardAvoidingView,
  ListRenderItem,
  Platform,
  StyleProp,
  ViewStyle,
} from 'react-native';

import ProductListItem from '../ProductListItem';
import styles from './styles';
import useController from './useController';
import { EAppStackPages } from '../../../navigation/routes';
import { TProduct } from '../../../duck/productAll/types';
import { TScreenNavigationProp, TProductListScreenProp } from './types';

export default (props: TProductListScreenProp) => {
  const navigation = useNavigation<TScreenNavigationProp>();
  const headerHeight = useHeaderHeight();

  const {
    prodFavList,
    addToCart,
    addToFavorites,
    removeFromCart,
    removeFromFavorites,
  } = props;

  const { cart, data, ListEmptyComponent, contentContainerStyle } =
    useController(props);

  /** Render item */
  const renderItem: ListRenderItem<TProduct> = ({ item, index }) => {
    const containerStyle: StyleProp<ViewStyle> =
      index % 2 ? styles.containeRightColumn : styles.containeLeftColumn;

    const isFavorite = prodFavList.findIndex((el) => el === item.id) >= 0;
    const qtyInCart = cart[item.id] ? cart[item.id].qty : 0;

    const onPress = () => {
      navigation.navigate(EAppStackPages.ProductDetailsScreen, {
        productId: item.id,
      });
    };

    const onAddToFavorites = () => addToFavorites(item.id);
    const onRemoveFromFavorites = () => removeFromFavorites(item.id);
    const onAddToCart = () => addToCart(item.id, 1);
    const onRemoveFromCart = () => removeFromCart(item.id, 1);

    return (
      <ProductListItem
        containerStyle={containerStyle}
        favorite={isFavorite}
        id={item.id}
        image={item.image}
        name={item.name}
        qtyInCart={qtyInCart}
        onAddToCart={onAddToCart}
        onAddToFavorites={onAddToFavorites}
        onPressImage={onPress}
        onRemoveFromCart={onRemoveFromCart}
        onRemoveFromFavorites={onRemoveFromFavorites}
        price={item.price}
      />
    );
  };

  return (
    <SafeAreaView style={styles.screen} edges={['left', 'right']}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        keyboardVerticalOffset={headerHeight}
      >
        <FlatList
          contentContainerStyle={contentContainerStyle}
          data={data}
          initialNumToRender={8}
          keyExtractor={(item) => _.toString(item.id)}
          numColumns={2}
          renderItem={renderItem}
          style={styles.list}
          ListEmptyComponent={ListEmptyComponent}
          testID="product-list"
        />
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};
