import { StyleSheet } from 'react-native';
import { Metrics, Layout } from '../../../styles';

export default StyleSheet.create({
  screen: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    flexGrow: 1,
    paddingTop: 16,
    width: Metrics.screenWidth,
  },
  list: {},
  containeLeftColumn: {
    marginLeft: Layout.baseMarging,
    marginRight: Layout.baseMarging / 2,
  },
  containeRightColumn: {
    marginLeft: Layout.baseMarging / 2,
    marginRight: Layout.baseMarging,
  },
});
