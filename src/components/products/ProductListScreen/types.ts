import { StackNavigationProp } from '@react-navigation/stack';

import { EAppStackPages } from '../../../navigation/routes';
import { TAppStackParamList } from '../../../navigation/types';
import { TState as TCartState } from '../../../duck/cart/types';
import { TState as TProductsAllState } from '../../../duck/productAll/types';
import { EdgeInsets } from 'react-native-safe-area-context';

export type TScreenNavigationProp = StackNavigationProp<
  TAppStackParamList,
  EAppStackPages.ProductListScreen
>;

export type TProductListScreenProp = {
  addToCart: (prodId: number, qty: 1) => void;
  addToFavorites: (prodId: number) => void;
  cart: TCartState;
  getProduct: (text: string) => void;
  headerHeight: number;
  navigation: TScreenNavigationProp;
  prodAllList: TProductsAllState['data'];
  prodAllPending: boolean;
  prodFavList: number[];
  removeFromCart: (prodId: number, qty: number) => void;
  removeFromFavorites: (prodId: number) => void;
  safeInsets: EdgeInsets;
};
