import { useEffect, useState } from 'react';
import { StyleProp, ViewStyle } from 'react-native';

import ProductListEmpty from '../ProductEmptyList';
import ProductListLoading from '../ProductListLoading';
import styles from './styles';
import { TProductListScreenProp } from './types';

export default ({
  cart,
  navigation,
  prodAllList,
  prodAllPending,
  prodFavList,
  safeInsets,
  getProduct,
}: TProductListScreenProp) => {
  const [initLoading, setInitLoading] = useState<
    'notLoaded' | 'loading' | 'loaded'
  >('notLoaded');

  const [favProdShown, setFavProdShown] = useState<boolean>(false);
  const [favListCopy, setFavListCopy] = useState<number[]>([]);
  const [searchText, setSearchText] = useState<string>('');

  const { byId, allIds } = prodAllList;
  const prodsAll = allIds.map((id) => byId[id]);
  const prodsFav = favListCopy.map((id) => byId[id]);

  const data = favProdShown ? prodsFav : prodsAll;
  const qtyProdsInCart = Object.keys(cart).length;

  const ListEmptyComponent =
    initLoading === 'loaded' ? ProductListEmpty : ProductListLoading;

  const contentContainerStyle: StyleProp<ViewStyle> = [
    styles.listContainer,
    { paddingBottom: safeInsets.bottom },
  ];

  // Get products
  useEffect(() => {
    getProduct(searchText);

    if (initLoading === 'notLoaded') {
      setInitLoading('loading');
    }
  }, [searchText]);

  // Finish init loading
  useEffect(() => {
    if (!prodAllPending && initLoading === 'loading') {
      setInitLoading('loaded');
    }
  }, [prodAllPending]);

  // Switch between all and favorite products
  useEffect(() => {
    const toggleFavProdVisibility = () => {
      setFavProdShown((prevState) => !prevState);
    };

    navigation.setParams({
      toggleFavProdVisibility,
      favProdShown,
    });

    if (favProdShown) {
      setFavListCopy([...prodFavList]);
    }
  }, [favProdShown, setFavProdShown, navigation]);

  // Handle search bar
  useEffect(() => {
    const onChangeSearchText = (text: string) => {
      setSearchText(text);
    };

    const onClearSearchText = () => {
      setSearchText('');
    };

    navigation.setParams({
      searchText,
      onChangeSearchText,
      onClearSearchText,
    });
  }, [searchText]);

  // Handle cart button
  useEffect(() => {
    navigation.setParams({ qtyProdsInCart });
  }, [qtyProdsInCart]);

  return {
    ListEmptyComponent,
    cart,
    contentContainerStyle,
    data,
    navigation,
  };
};
