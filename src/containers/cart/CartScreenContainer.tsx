import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import CartScreen from '../../components/cart/CartScreen';
import { useAppDispatch, useAppSelector } from '../../utils/hooks';
import { actions as cartActions } from '../../duck/cart';

import {
  TScreenNavigationProp,
  TCartScreenProp,
} from '../../components/cart/CartScreen/types';

export default () => {
  const dispatch = useAppDispatch();
  const safeInsets = useSafeAreaInsets();
  const navigation = useNavigation<TScreenNavigationProp>();

  const cart = useAppSelector((state) => state.cart);
  const { data: prodAllList } = useAppSelector((state) => state.productsAll);

  const addToCart: TCartScreenProp['addToCart'] = (prodId, qty) => {
    dispatch(cartActions.addProductToCart({ prodId, qty }));
  };

  const removeFromCart: TCartScreenProp['removeFromCart'] = (prodId, qty) => {
    dispatch(cartActions.removeProductFromCart({ prodId, qty }));
  };

  return (
    <CartScreen
      addToCart={addToCart}
      cart={cart}
      navigation={navigation}
      prodAllList={prodAllList}
      removeFromCart={removeFromCart}
      safeInsets={safeInsets}
    />
  );
};
