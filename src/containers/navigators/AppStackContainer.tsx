import React from 'react';

import AppStack from '../../components/navigators/AppStack';
import { useAppSelector } from '../../utils/hooks';

export default () => {
  const cart = useAppSelector((state) => state.cart);

  const {
    meta: { pending: prodAllPending },
  } = useAppSelector((state) => state.productsAll);

  return <AppStack cart={cart} prodAllPending={prodAllPending} />;
};
