import React from 'react';
import { useRoute } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import ProductDetailsScreen from '../../components/products/ProductDetailsScreen';
import { useAppDispatch, useAppSelector } from '../../utils/hooks';
import { actions as cartActions } from '../../duck/cart';
import { actions as favoriteProductActions } from '../../duck/productFavorites';

import {
  TScreenRouteProp,
  TProductDetailsScreenProp,
} from '../../components/products/ProductDetailsScreen/types';

export default () => {
  const dispatch = useAppDispatch();
  const route = useRoute<TScreenRouteProp>();
  const safeInsets = useSafeAreaInsets();

  const cart = useAppSelector((state) => state.cart);
  const { data: prodAllList } = useAppSelector((state) => state.productsAll);

  const prodFavList = useAppSelector(
    (state) => state.productsFavorite.data.allIds,
  );

  const addToFavorites: TProductDetailsScreenProp['addToFavorites'] = (
    prodId,
  ) => {
    dispatch(favoriteProductActions.addProductToFavorites({ id: prodId }));
  };

  const removeFromFavorites: TProductDetailsScreenProp['removeFromFavorites'] =
    (prodId) => {
      dispatch(
        favoriteProductActions.removeProductFromFavorites({ id: prodId }),
      );
    };

  const addToCart: TProductDetailsScreenProp['addToCart'] = (prodId, qty) => {
    dispatch(cartActions.addProductToCart({ prodId, qty }));
  };

  const removeFromCart: TProductDetailsScreenProp['removeFromCart'] = (
    prodId,
    qty,
  ) => {
    dispatch(cartActions.removeProductFromCart({ prodId, qty }));
  };

  return (
    <ProductDetailsScreen
      addToCart={addToCart}
      addToFavorites={addToFavorites}
      cart={cart}
      prodAllList={prodAllList}
      prodFavList={prodFavList}
      removeFromCart={removeFromCart}
      removeFromFavorites={removeFromFavorites}
      route={route}
      safeInsets={safeInsets}
    />
  );
};
