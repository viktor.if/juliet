import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { useHeaderHeight } from '@react-navigation/elements';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import ProductListScreen from '../../components/products/ProductListScreen';
import { useAppDispatch, useAppSelector } from '../../utils/hooks';
import { actions as cartActions } from '../../duck/cart';
import { actions as favoriteProductActions } from '../../duck/productFavorites';
import { actions as productActions } from '../../duck/productAll';

import {
  TScreenNavigationProp,
  TProductListScreenProp,
} from '../../components/products/ProductListScreen/types';

export default () => {
  const dispatch = useAppDispatch();
  const headerHeight = useHeaderHeight();
  const navigation = useNavigation<TScreenNavigationProp>();
  const safeInsets = useSafeAreaInsets();

  const cart = useAppSelector((state) => state.cart);
  const prodFavList = useAppSelector(
    (state) => state.productsFavorite.data.allIds,
  );

  const {
    data: prodAllList,
    meta: { pending: prodAllPending },
  } = useAppSelector((state) => state.productsAll);

  const addToFavorites: TProductListScreenProp['addToFavorites'] = (prodId) => {
    dispatch(favoriteProductActions.addProductToFavorites({ id: prodId }));
  };

  const removeFromFavorites: TProductListScreenProp['removeFromFavorites'] = (
    prodId,
  ) => {
    dispatch(favoriteProductActions.removeProductFromFavorites({ id: prodId }));
  };

  const addToCart: TProductListScreenProp['addToCart'] = (prodId, qty) => {
    dispatch(cartActions.addProductToCart({ prodId, qty }));
  };

  const removeFromCart: TProductListScreenProp['removeFromCart'] = (
    prodId,
    qty,
  ) => {
    dispatch(cartActions.removeProductFromCart({ prodId, qty }));
  };

  const getProduct: TProductListScreenProp['getProduct'] = (
    searchText: string,
  ) => {
    dispatch(productActions.getProductsRequest({ query: searchText }));
  };

  return (
    <ProductListScreen
      addToCart={addToCart}
      addToFavorites={addToFavorites}
      cart={cart}
      getProduct={getProduct}
      headerHeight={headerHeight}
      navigation={navigation}
      prodAllList={prodAllList}
      prodAllPending={prodAllPending}
      prodFavList={prodFavList}
      removeFromCart={removeFromCart}
      removeFromFavorites={removeFromFavorites}
      safeInsets={safeInsets}
    />
  );
};
