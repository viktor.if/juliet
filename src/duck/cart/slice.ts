import { createSlice } from '@reduxjs/toolkit';
import _ from 'lodash';

import { CART } from '../../utils/constants';
import { TAddProductToCart, TRemoveProductFromCart, TState } from './types';

const cleanState: TState = {};
const name = CART;
const initialState = _.cloneDeep(cleanState);

const addProductToCart: TAddProductToCart = (state, { payload }) => {
  if (state[payload.prodId]) {
    state[payload.prodId].qty += payload.qty;
  } else {
    state[payload.prodId] = {
      id: payload.prodId,
      qty: payload.qty,
    };
  }
};

const removeProductFromCart: TRemoveProductFromCart = (state, { payload }) => {
  if (state[payload.prodId] && state[payload.prodId].qty - payload.qty <= 0) {
    delete state[payload.prodId];
  } else if (state[payload.prodId]) {
    state[payload.prodId].qty -= payload.qty;
  }
};

const reducers = {
  addProductToCart,
  removeProductFromCart,
};

export default createSlice({ name, initialState, reducers });
