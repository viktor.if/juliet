import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';

export type TProduct = {
  created_at: string;
  description: string;
  id: number;
  image: string;
  in_stock: boolean;
  name: string;
  price: number;
};

export type TState = {
  [key: number]: {
    id: number;
    qty: number;
  };
};

/** Add product to cart */
type TAddProductToCartAction = PayloadAction<{
  qty: number;
  prodId: number;
}>;

export type TAddProductToCart = CaseReducer<TState, TAddProductToCartAction>;

/** Remove product from cart */
type TRemoveProductFromCartAction = PayloadAction<{
  qty: number;
  prodId: number;
}>;

export type TRemoveProductFromCart = CaseReducer<
  TState,
  TRemoveProductFromCartAction
>;
