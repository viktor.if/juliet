import { createSlice } from '@reduxjs/toolkit';
import _ from 'lodash';
import { normalize, schema } from 'normalizr';

import { PRODUCT, PRODUCTS_ALL } from '../../utils/constants';

import {
  TGetProductsFailure,
  TGetProductsRequest,
  TGetProductsSuccess,
  TProduct,
  TState,
} from './types';

const cleanState: TState = {
  data: {
    allIds: [],
    byId: {},
  },
  meta: {
    count: 0,
    error: null,
    pending: false,
  },
};

const name = PRODUCTS_ALL;
const initialState = _.cloneDeep(cleanState);

const getProductsRequest: TGetProductsRequest = (state) => {
  state.meta.pending = true;
  state.meta.error = null;
};

const getProductsFailure: TGetProductsFailure = (state, { payload }) => {
  state.meta.pending = false;
  state.meta.error = payload.error;
};

const getProductsSuccess: TGetProductsSuccess = (state, { payload }) => {
  state.meta.pending = false;

  state.meta.count = payload.count;

  const productSchema = new schema.Entity<TProduct>(PRODUCT);
  const productListSchema = [productSchema];
  const normalized = normalize<TProduct>(payload.data, productListSchema);

  state.data.byId = Object.assign(
    state.data.byId,
    normalized.entities[PRODUCT] || {},
  );

  state.data.allIds = normalized.result;
};

const reducers = {
  getProductsRequest,
  getProductsFailure,
  getProductsSuccess,
};

export default createSlice({ name, initialState, reducers });
