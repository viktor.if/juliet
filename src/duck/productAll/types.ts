import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';

export type TProduct = {
  created_at: string;
  description: string;
  id: number;
  image: string;
  in_stock: boolean;
  name: string;
  price: number;
};

export type TState = {
  data: {
    allIds: number[];
    byId: {
      [key: number]: TProduct;
    };
  };
  meta: {
    count: number;
    error: string | null;
    pending: boolean;
  };
};

/** Get products */
type TGetProductsRequestAction = PayloadAction<{
  query: string;
}>;

export type TGetProductsRequest = CaseReducer<
  TState,
  TGetProductsRequestAction
>;

type TGetProductsFailureAction = PayloadAction<{
  error: string;
}>;

export type TGetProductsFailure = CaseReducer<
  TState,
  TGetProductsFailureAction
>;

type TGetProductsSuccessAction = PayloadAction<{
  data: TProduct[];
  count: number;
}>;

export type TGetProductsSuccess = CaseReducer<
  TState,
  TGetProductsSuccessAction
>;
