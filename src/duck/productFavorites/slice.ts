import { createSlice } from '@reduxjs/toolkit';
import _ from 'lodash';

import { PRODUCTS_FAVORITE } from '../../utils/constants';

import {
  TAddProductToFavorites,
  TRemoveProductFromFavorites,
  TState,
} from './types';

const cleanState: TState = {
  data: {
    allIds: [],
  },
};

const name = PRODUCTS_FAVORITE;
const initialState = _.cloneDeep(cleanState);

const addProductToFavorites: TAddProductToFavorites = (state, { payload }) => {
  state.data.allIds.push(payload.id);
};

const removeProductFromFavorites: TRemoveProductFromFavorites = (
  state,
  { payload },
) => {
  state.data.allIds = state.data.allIds.filter((item) => item !== payload.id);
};

const reducers = {
  addProductToFavorites,
  removeProductFromFavorites,
};

export default createSlice({ name, initialState, reducers });
