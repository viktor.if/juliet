import { PayloadAction, CaseReducer } from '@reduxjs/toolkit';

export type TProduct = {
  created_at: string;
  description: string;
  id: number;
  image: string;
  in_stock: boolean;
  price: number;
};

export type TState = {
  data: {
    allIds: number[];
  };
};

/** Add a product to the favorites */
type TAddProductToFavoritesAction = PayloadAction<{
  id: number;
}>;

export type TAddProductToFavorites = CaseReducer<
  TState,
  TAddProductToFavoritesAction
>;

/** Remove a product from the favorites */
type TRemoveProductFromFavoritesAction = PayloadAction<{
  id: number;
}>;

export type TRemoveProductFromFavorites = CaseReducer<
  TState,
  TRemoveProductFromFavoritesAction
>;
