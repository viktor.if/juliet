export enum EAppStackPages {
  ProductListScreen = 'ProductListScreen',
  ProductDetailsScreen = 'ProductDetailsScreen',
  CartScreen = 'CartScreen',
}
