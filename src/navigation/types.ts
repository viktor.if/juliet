import { EAppStackPages } from './routes';

export type TAppStackParamList = {
  [EAppStackPages.ProductListScreen]: {
    favProdShown: boolean;
    onChangeSearchText: (text: string) => void;
    onClearSearchText: () => void;
    qtyProdsInCart: number;
    searchText: string;
    toggleFavProdVisibility: () => void;
  };
  [EAppStackPages.ProductDetailsScreen]: {
    productId: number;
  };
  [EAppStackPages.CartScreen]: undefined;
};
