import { takeLatest, put, call, delay } from 'redux-saga/effects';

import { actions } from '../duck/productAll';
import ProductsApi from '../api/Products';
import { Await } from '../utils/types';

export function* getProductsSaga({
  payload: { query },
}: ReturnType<typeof actions.getProductsRequest>) {
  try {
    yield delay(500);

    const { data, count, error }: Await<ReturnType<typeof ProductsApi.getAll>> =
      yield call<typeof ProductsApi.getAll>(ProductsApi.getAll, { query });

    if (error) {
      throw error;
    }

    yield put(actions.getProductsSuccess({ data, count }));
  } catch (error: any) {
    yield put(
      actions.getProductsFailure({
        error: error.message ? error.message : 'Error occurred',
      }),
    );
  }
}

function* productsAllSaga() {
  yield takeLatest(actions.getProductsRequest, getProductsSaga);
}

export default productsAllSaga;
