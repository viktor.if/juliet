import { all } from 'redux-saga/effects';

import productsAllSaga from './productsAllSaga';

function* rootSaga() {
  yield all([productsAllSaga()]);
}

export default rootSaga;
