import AsyncStorage from '@react-native-async-storage/async-storage';
import createSagaMiddleware from 'redux-saga';

import {
  configureStore,
  combineReducers,
  CombinedState,
} from '@reduxjs/toolkit';

import {
  persistReducer,
  persistStore,
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  PersistConfig,
} from 'redux-persist';

import rootSaga from './sagas/rootSaga';
import { PRODUCTS_ALL, PRODUCTS_FAVORITE, ROOT, CART } from './utils/constants';

/** Reducers */
import { reducer as productsAllReducer } from './duck/productAll';
import { reducer as productsFavoriteReducer } from './duck/productFavorites';
import { reducer as cartReducer } from './duck/cart';

/** States */
import { TState as ProductsAllState } from './duck/productAll/types';
import { TState as ProductsFavoriteState } from './duck/productFavorites/types';
import { TState as CartState } from './duck/cart/types';

const rootReducer = combineReducers({
  [CART]: cartReducer,
  [PRODUCTS_ALL]: productsAllReducer,
  [PRODUCTS_FAVORITE]: productsFavoriteReducer,
});

const persistConfig: PersistConfig<
  CombinedState<{
    [CART]: CartState;
    [PRODUCTS_ALL]: ProductsAllState;
    [PRODUCTS_FAVORITE]: ProductsFavoriteState;
  }>
> = {
  key: ROOT,
  storage: AsyncStorage,
  whitelist: [PRODUCTS_FAVORITE, CART],
};

const reducer = persistReducer(persistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware();
const devTools = __DEV__;

const defaultMiddlewareConfig = {
  thunk: false,
  serializableCheck: {
    ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
  },
};

export const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => {
    const middlewares = getDefaultMiddleware(defaultMiddlewareConfig).concat(
      sagaMiddleware,
    );

    return middlewares;
  },
  devTools,
});

export const { dispatch, getState } = store;
export const persistor = persistStore(store);
export type TStoreState = ReturnType<typeof store.getState>;

sagaMiddleware.run(rootSaga);
