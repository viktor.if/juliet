export const PRODUCTS_ALL = 'productsAll';
export const PRODUCTS_FAVORITE = 'productsFavorite';
export const PRODUCTS = 'products';
export const ROOT = 'root';
export const PRODUCT = 'product';
export const CART = 'cart';
export const BASE_SCREEN_HEIGHT = 785;
