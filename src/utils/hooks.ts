import { useEffect } from 'react';
import { useSelector, TypedUseSelectorHook, useDispatch } from 'react-redux';
import { store, TStoreState } from '../store';

type AppDispatch = typeof store.dispatch;

/** Dispatch */
export const useAppDispatch = () => useDispatch<AppDispatch>();

/** Selector */
export const useAppSelector: TypedUseSelectorHook<TStoreState> = useSelector;

/** Mount */
// eslint-disable-next-line react-hooks/exhaustive-deps
export const useMount = (func: () => void) => useEffect(() => func(), []);
