import { LogBox } from 'react-native';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
  'Require cycle: node_modules/@supabase',
  'Non-serializable values were found in the navigation state',
  'EventEmitter.removeListener',
]);
