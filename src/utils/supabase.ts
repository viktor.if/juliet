import { createClient } from '@supabase/supabase-js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Config from 'react-native-config';

export const supabase = createClient(Config.SUPABASE_URL, Config.SUPABASE_KEY, {
  localStorage: AsyncStorage as any,
});
