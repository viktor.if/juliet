import { AppRegistry } from 'react-native';
import { withKnobs } from '@storybook/addon-knobs';

import {
  addDecorator,
  configure,
  getStorybookUI,
} from '@storybook/react-native';

import { loadStories } from './storyLoader';
import './rn-addons';

addDecorator(withKnobs);

configure(() => {
  loadStories();
}, module);

const StorybookUIRoot = getStorybookUI({
  asyncStorage: require('@react-native-async-storage/async-storage').default,
});

AppRegistry.registerComponent('%APP_NAME%', () => StorybookUIRoot);

export default StorybookUIRoot;
