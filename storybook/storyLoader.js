// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/components/buttons/ButtonRounded/ButtonRounded.stories');
  require('../src/components/buttons/HeaderArrowLeftButton/HeaderArrowLeftButton.stories');
  require('../src/components/buttons/HeaderCartButton/HeaderCartButton.stories');
  require('../src/components/buttons/HeaderCloseButton/HeaderCloseButton.stories');
  require('../src/components/buttons/HeaderFavoritesButton/HeaderFavoritesButton.stories');
  require('../src/components/buttons/HeaderHomeButton/HeaderHomeButton.stories');
  require('../src/components/cart/CartCheckoutBar/CartCheckoutBar.stories');
  require('../src/components/cart/CartItemSeparator/CartItemSeparator.stories');
  require('../src/components/cart/CartListItem/CartListItem.stories');
  require('../src/components/common/IconList/HeaderProductList.stories');
  require('../src/components/headers/HeaderCart/HeaderCart.stories');
  require('../src/components/headers/HeaderProductDetails/HeaderProductDetails.stories');
  require('../src/components/headers/HeaderProductList/HeaderProductList.stories');
  require('../src/components/products/ProductDetailsScreen/ProductDetailsScreen.stories');
  require('../src/components/products/ProductListItem/ProductListItem.stories');
}

const stories = [
  '../src/components/buttons/ButtonRounded/ButtonRounded.stories',
  '../src/components/buttons/HeaderArrowLeftButton/HeaderArrowLeftButton.stories',
  '../src/components/buttons/HeaderCartButton/HeaderCartButton.stories',
  '../src/components/buttons/HeaderCloseButton/HeaderCloseButton.stories',
  '../src/components/buttons/HeaderFavoritesButton/HeaderFavoritesButton.stories',
  '../src/components/buttons/HeaderHomeButton/HeaderHomeButton.stories',
  '../src/components/cart/CartCheckoutBar/CartCheckoutBar.stories',
  '../src/components/cart/CartItemSeparator/CartItemSeparator.stories',
  '../src/components/cart/CartListItem/CartListItem.stories',
  '../src/components/common/IconList/HeaderProductList.stories',
  '../src/components/headers/HeaderCart/HeaderCart.stories',
  '../src/components/headers/HeaderProductDetails/HeaderProductDetails.stories',
  '../src/components/headers/HeaderProductList/HeaderProductList.stories',
  '../src/components/products/ProductDetailsScreen/ProductDetailsScreen.stories',
  '../src/components/products/ProductListItem/ProductListItem.stories',
];

module.exports = {
  loadStories,
  stories,
};
